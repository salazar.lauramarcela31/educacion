<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?php 
        if ($model->isNewRecord) {
            echo $form->field($model, 'password')->passwordInput(['maxlength' => true]);
        }
     ?>

    <?= $form->field($model, 'rol')->dropDownList([1 => 'Administrador', 2 => 'Operativo']); ?>

    <?= $form->field($model, 'estado')->dropDownList([1 => 'Activo', 0 => 'Inactivo']); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
