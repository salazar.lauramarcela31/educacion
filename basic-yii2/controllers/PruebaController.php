<?php

namespace app\controllers;
use yii\web\Controller;

use Yii;

class PruebaController extends Controller{

    public function actionIndex(){
        return $this->render('index');
    }
    public function actionNuevaCuenta(){
        return $this->render('nueva-cuenta');
    }
}