<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property int $id_user
 * @property string $date
 * @property int $id_product_category
 * @property int $amount_points
 * @property string $instructions_use
 * @property string $details
 * @property string $terms
 * @property string $image
 * @property int $status
 *
 * @property ProductCategory $productCategory
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'id_user', 'date', 'id_product_category', 'amount_points', 'instructions_use', 'details', 'terms', 'image'], 'required'],
            [['id_user', 'id_product_category', 'amount_points', 'status'], 'integer'],
            [['date'], 'safe'],
            [['name'], 'string', 'max' => 80],
            [['instructions_use', 'details', 'terms'], 'string', 'max' => 800],
            [['image'], 'file', 'extensions' => 'jpg,jpeg,png'],
            [['image'], 'string', 'max' => 300],
            [['id_product_category'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::class, 'targetAttribute' => ['id_product_category' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'id_user' => Yii::t('app', 'Id User'),
            'date' => Yii::t('app', 'Date'),
            'id_product_category' => Yii::t('app', 'Id Product Category'),
            'amount_points' => Yii::t('app', 'Amount Points'),
            'instructions_use' => Yii::t('app', 'Instructions Use'),
            'details' => Yii::t('app', 'Details'),
            'terms' => Yii::t('app', 'Terms'),
            'image' => Yii::t('app', 'Image'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * Gets query for [[ProductCategory]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategory()
    {
        return $this->hasOne(ProductCategory::class, ['id' => 'id_product_category']);
    }

    
}
